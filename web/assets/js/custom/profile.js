$(document).ready(function()
{
    //alert(URL);
    var ias = jQuery.ias({
            container: '.profile-box #user_publications',         //Contenedor de los usuarios/item
            item: '.publication-item',             //Item a paginar, clase user-item
            pagination: '.profile-box .pagination',      //Controles de paginacion dentro de .pagination
            next: '.profile-box .pagination .next_link', //Peticion siguiente a Paginar
            triggerPageThreshold: 5         //Frecuencia de paginacion
    });
    
    //Maximo punto de paginacion
    ias.extension(new IASTriggerExtension({
        text: 'Ver más publicaciones',
        offset: 3
    }));
    
    //Imagen de carga
    ias.extension(new IASSpinnerExtension({
        src: URL+'/../assets/images/ajax-loader.gif'
    }));

    //Texto ya no hay más que mostrar
    ias.extension(new IASNoneLeftExtension({
        text: 'No hay más publicaciones'
    }));
    
    ias.on('ready', function(event){
        buttons();
		followButtons();
    });
    
    ias.on('rendered', function(event){
        buttons();
    });
});

function buttons()
{
    $('[data-toggle="tooltip"]').tooltip();
    $(".btn-img").unbind("click").click(function()
    {
        $(this).parent().find('.pub-image').fadeToggle();
    });
    
    $(".btn-delete-pub").unbind("click").click(function()
    {
        $(this).parent().parent().addClass('hidden');
        
        $.ajax({
            url: URL+'/publication/remove/'+$(this).attr("data-id"),
            type: 'GET',
            success: function(response)
            {
                console.log(response);
            }
        });
    });
    
    $(".btn-like").unbind("click").click(function()
    {
        $(this).addClass("hidden");
        $(this).parent().find('.btn-unlike').removeClass("hidden");
        $.ajax({
            url: URL+'/like/'+$(this).attr("data-id"),
            type: 'GET',
            success: function(response)
            {
                console.log(response);
            }
        });
    });
}