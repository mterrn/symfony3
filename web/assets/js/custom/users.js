/*$(document).ready(function()
{
    alert("USERS");
}
);*/

$(document).ready(function()
{
    //alert(URL);
    var ias = jQuery.ias({
            container: '.box-users',         //Contenedor de los usuarios/item
            item: '.user-item',             //Item a paginar, clase user-item
            pagination: '.pagination',      //Controles de paginacion dentro de .pagination
            next: '.pagination .next_link', //Peticion siguiente a Paginar
            triggerPageThreshold: 5         //Frecuencia de paginacion
    });
    
    //Maximo punto de paginacion
    ias.extension(new IASTriggerExtension({
        text: 'Ver más personas',
        offset: 3
    }));
    
    //Imagen de carga
    ias.extension(new IASSpinnerExtension({
        src: URL+'/../assets/images/ajax-loader.gif'
    }));

    //Texto ya no hay más que mostrar
    ias.extension(new IASNoneLeftExtension({
        text: 'No hay más personas'
    }));
    
    ias.on('ready', function(event){
        followButtons();
    });
    
    ias.on('rendered', function(event){
        followButtons();
    });
});